const express = require("express");
const bodyParser = require('body-parser')
const cors = require('cors');

const app = express();
const PORT = 3000;

app.use(cors());
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get('/connection', (req, res)=>{
  res.send('Connected');
});
app.listen(PORT, ()=>{
  console.log('server started on port:'+PORT);
});